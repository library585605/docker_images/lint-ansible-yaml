FROM python:3-slim

RUN export ANSIBLE_HOST_KEY_CHECKING=False &&\	    
    python3 -m pip install --no-cache-dir --upgrade ansible-core==2.16.3 ansible-lint yamllint --break-system-packages &&\
    apt-get clean &&\
    ansible-lint --version && yamllint --version

